C       PROGRAM TO FIND THE AREA OF THE TRIANGLE
        PROGRAM AREAOFTRIANGLE
        REAL AREA, A, B, C, S
        WRITE(*,*) 'ENTER THE SIDES OF THE TRIANGLE'
C       GET INPUT FOR THREE SIDES OF TRIANGLE
        READ *, A, B, C
C       CALCULATING AREA OF THE TRIANGLE
        S = (A + B + C) / 2
        AREA = SQRT(S*(S-A)*(S-B)*(S-C))
        WRITE (*,*) 'The area of the triangle is', AREA, 'square units.'
        END