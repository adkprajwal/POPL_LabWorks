C       PROGRAM TO FIND THE AREA OF TRIANGLE USING FUNCTION
        PROGRAM AREAFUNCTION
        REAL AREA, A, B, C
        WRITE(*,*) 'ENTER THE SIDES OF A TRIANGLE'
C       GET INPUT FOR THREE SIDES OF TRIANGLE
        READ *, A, B, C
        AREA = AREAOFTRIANGLE(A, B, C)
        WRITE (*,*) 'The area of the triangle is', AREA, 'square units.'
        END
C       FUNCTION_TO_CALCULATE AREA OF THE TRIANGLE
        REAL FUNCTION AREAOFTRIANGLE(X, Y, Z)
        REAL X, Y, Z, S
        S = (X + Y + Z) / 2.0
        AREAOFTRIANGLE = SQRT(S*(S-X)*(S-Y)*(S-Z))
        RETURN
        END